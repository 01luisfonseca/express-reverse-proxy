require("dotenv").config();

var express = require("express");
var cors = require("cors");
var axios = require("axios");

var router = express.Router();

router.use(cors());

router.use((req, res, next) => {
  if (!req.headers["x-ap-host"]) return res.status(400).json({
    message: "Request require a valid x-ap-host header"
  });
  req.XAPPHost = req.headers["x-ap-host"];
  delete req.headers["x-ap-host"]
  delete req.headers["host"]
  next();
});

router.use((req, res, next) => {
  req.XAPPTime = Date.now();
  next();
});

router.use(async (req, res) => {
  try {
    if (req.headers["accept-encoding"]) delete req.headers["accept-encoding"]
    const response = await axios({
      method: req.method,
      url: req.XAPPHost,
      data: req.rawBody,
      timeout: 60000,
      headers: req.headers
    })
    if (response.headers["content-type"]) res.set("content-type", response.headers["content-type"]);
    res.set("X-AP-HostTime", `${Date.now() - req.XAPPTime}`);
    res.status(response.status).send( response.data );
  } catch (error) {
    res.status(500).json({
      message: "Error in external host request",
      error
    })
  }
});

module.exports = router;
