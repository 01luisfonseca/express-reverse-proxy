# express-reverse-proxy

Implementation of reverse proxy with Nodejs / Express. Created to handle HTTP verbs and mantain parameters and request headers.

The main objetive is skip CORS in request, useful in secure knowed servers.

## Usage

This module require Express in your main implementation. This module returns an Express Router, for your express implementation.

```javascript
// Typical express declaration...

var RouterAP = require("express-reverse-api-proxy");

// Other Express middlewares and routes...

app.use("/api_proxy_endpoint", RouterAP);

```

Every request to endpoint *REQUIRE* this header: 
* x-ap-host -> String with URL of destiny

And *REQUIRE* an middleware conversion to string, same as next:

```javascript
const express = require("express");
const app = express();

// Other express declarations and middleware

app.use((req, res, next) => {
  var data = "";
  req.setEncoding("utf8");
  req.on("data", (chunk) => {
    data += chunk;
  });

  req.on("end", () => {
    req.rawBody = data; // Attaching special property rawBody. Is a string of request.
    next();
  });
});
```

This middleware allows data to be passed to the server without unnecesary conversions.

The response is the same response of original service. test it in Postman or other request tester.
